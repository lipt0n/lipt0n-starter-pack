const {
    FuseBox,
    WebIndexPlugin ,
    CSSPlugin,
    SassPlugin,
    BabelPlugin,
    CSSResourcePlugin } = require("fuse-box")
const  GraphQLPlugin  =  require('fuse-box-graphql-plugin')
const fuse = FuseBox.init({
    homeDir : "src",
    output : "dist/$name.js",
    useTypescriptCompiler : true,
    alias: {client: '~/client', server: '~/server'  },
    sourceMaps: { project: true, vendor: false },  // VS debuging, sourcemaps not needed
    plugins: [
        WebIndexPlugin({
           bundles:["client/app",],
            template : "src/client/index.html"
        }),

        ['.graphql|.gql', GraphQLPlugin()],
        [SassPlugin({importer : true}), CSSResourcePlugin({ dist: "dist/css-resources" }), CSSPlugin()]
,
        [ BabelPlugin({
         presets: ["es2015", "stage-0"],
         limit2project:true
       })],
      ]
})

fuse.dev({
    port : 9001,
    httpServer: false
})




fuse.bundle("server/bundle")
    .watch("server/**") // watch only server related code.. bugs up atm
    .target('server')
    .instructions(" > [server/index.ts]")
  //  .hmr()
    // Execute process right after bundling is completed
    // launch and restart express
    .completed(proc => proc.start())
     /*
    .completed((proc) => {
        proc.require({
            close: ({ FuseBox }) => FuseBox.import(FuseBox.mainFile).shutdown()
        })
    })
    */

fuse.bundle("client/app")
    .watch("client/**") // watch only client related code
    .target('browser')
    .hmr({socketURI: "ws://localhost:9001"})
    .instructions(" > client/index.tsx")

fuse.run()
