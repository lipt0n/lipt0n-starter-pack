import * as express from "express"
import * as path from "path"
import * as bodyParser from 'body-parser'
import { graphqlExpress,graphiqlExpress } from 'apollo-server-express'
import { SubscriptionServer } from 'subscriptions-transport-ws'
import { execute, subscribe } from 'graphql'
import { makeExecutableSchema  } from 'graphql-tools'
import { createServer } from 'http'
import "reflect-metadata"
import {createConnection} from "typeorm"

import {User,
   Resolver as userResolver,
   userTypes,
   QuerySchema as userQuerySchema,
   Mutations as userMutations,
   Subscriptions as userSubscriptions,
  MutationsSchema as userMutationsSchema,
  SubscriptionSchema as userSubscriptionSchema
  } from "./db/entity/User"

const app = express()

createConnection({
  type:'sqlite',
  database:'db.sqlite3',
  entities:[User,],
  synchronize:true,
  logging:false
}).then(async connection => {


    const resolvers = {
      Query: { ...userResolver },
      Mutation: { ...userMutations },
      Subscription: {
          ...userSubscriptions
      }
    }



  const typeDefs = [
      `
      schema {
          query: Query,
          mutation: Mutation,
          subscription:Subscription
      }
      `,
      `type Query {
        ${userQuerySchema}
      }
      type Mutation {
        ${userMutationsSchema}
      }
      type Subscription {
        ${userSubscriptionSchema}
      }
      `,
      ...userTypes,
  ]
    // Put together a schema
    const schema = makeExecutableSchema({
      typeDefs,
      resolvers,
    })

    const dist = path.resolve("./dist")
    app.use("/client", express.static(path.resolve("./dist/client")))
    app.use('/graphql', bodyParser.json(), graphqlExpress({ schema }))
    app.get('/graphiql', graphiqlExpress({ endpointURL: '/graphql' }))
    app.get("*", function(req, res) {
      res.sendFile(path.join(dist, "index.html"))
    })
    const server = http.createServer(app)
    server.listen(9000, function () {
        new SubscriptionServer({
            execute,
            subscribe,
            schema,
          }, {
            server,
            path: '/sub',
          })
        console.log('App listening on port 9000!')
    })
}).catch(error => console.log(error))
