import {Entity, PrimaryGeneratedColumn, Column, getRepository, BaseEntity } from "typeorm"
import { PubSub } from 'graphql-subscriptions'
const pubsub = new PubSub()
@Entity()
export class User extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    firstName: string

    @Column()
    lastName: string

    @Column()
    age: number

    @Column({
        default:false
    })
    verified: boolean

    @Column({ nullable: true })
    email: string


}

export const Resolver = {
    users : async () => User.find(),
}
export const Subscriptions = {
    users:{ subscribe:   ()=> {
      return pubsub.asyncIterator('users')
    }}
  }
export const Mutations = {
    createUser :   async (_, args) =>  {
      const user = await  User.create(args).save()
      pubsub.publish('users', {users: await User.find()})
      return user
      }


}
export const userTypes = [`
    type User {
        firstName: String,
        lastName: String,
        age:Int,
        verified:Boolean,
        email: String
    }
    `,]
export const QuerySchema = `
    users: [User]
    `
export const MutationsSchema = `
createUser( firstName:String!, lastName:String!, age:Int! ):User!
`
export const SubscriptionSchema = `
users:[User]
`
