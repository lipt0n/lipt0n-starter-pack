import * as WebFontLoader from 'webfontloader'
import * as React from 'react'
import * as ReactDOM from 'react-dom'
import  {Layout} from './pages/Layout'
import { BrowserRouter as Router } from 'react-router-dom'
import { apolloClient } from 'client/apolloClient'
import { ApolloProvider } from 'react-apollo'
import 'client/main.scss'

console.log('apolloClient:', apolloClient)
WebFontLoader.load({
    google: {
      families: ['Material Icons', 'Work Sans','Roboto:300,400,500,700'],
    },
  })
  ReactDOM.render(
             <ApolloProvider client={apolloClient}>
              <Router><Layout /></Router>
              </ApolloProvider>
        ,
  document.getElementById('app')  as HTMLElement
)