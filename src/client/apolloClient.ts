import { ApolloClient } from 'apollo-client'
import { HttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { SubscriptionClient } from 'subscriptions-transport-ws'
import { WebSocketLink } from 'apollo-link-ws'
import { getOperationAST } from 'graphql'
import { withClientState } from 'apollo-link-state'
import { ApolloLink } from 'apollo-link'
import { getMainDefinition } from 'apollo-utilities'

const GRAPHQL_ENDPOINT = 'ws://localhost:9000/sub'
const cache =  new InMemoryCache()
const stateLink = withClientState({
  cache,
  resolvers: {
    Mutation: {
      updateNetworkStatus: (_, { isConnected }, { cache }) => {
        const data = {
          networkStatus: {
            __typename: 'NetworkStatus',
            isConnected
          },
        };
        cache.writeData({ data })
        return null
      },
    },
  }
})
const httpLink = ApolloLink.from([stateLink,new HttpLink()])
const wsLink = new WebSocketLink({uri:GRAPHQL_ENDPOINT, options:{
  reconnect: true,
}
  }
)

const link = ApolloLink.split(
  // split based on operation type
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query)
    return kind === 'OperationDefinition' && operation === 'subscription'
  },
  wsLink,
  httpLink,
)
const apolloClient  = new ApolloClient({
  link,
  cache,
  connectToDevTools: true
})
export { apolloClient }