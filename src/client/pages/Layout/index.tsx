import * as React from "react"
import { BrowserRouter as Router, Route, Link,Switch } from "react-router-dom"
import { NavigationDrawer,FontIcon } from 'react-md'
import { withRouter } from 'react-router'
import { NavLink} from 'client/pages/Layout/NavLink'
import { Home } from 'client/pages/Home'
import {Users} from 'client/pages/Users'

const ToolbarTitle = ()=><div style={{marginTop:10}}><span>LIPT</span><FontIcon style={{
    color: '#f00',
    fontSize:25, position:'relative', top:5
    }}>power_settings_new</FontIcon><span>N</span> <span style={{}}>starter pack</span></div>
const navItems = [{
    label: 'Start',
    to: '/',
    exact: true,
    icon: 'home',
  }, {
    label: 'Users',
    to: '/users',
    icon: 'groups',
  }]

class Layout extends React.Component<any, any> {

  render():JSX.Element{

    return (
        <Route
        render={({ location }) => (
                                <NavigationDrawer
                                  toolbarTitle={<ToolbarTitle/>}
                                  mobileDrawerType={NavigationDrawer.DrawerTypes.TEMPORARY_MINI}
                                  tabletDrawerType={NavigationDrawer.DrawerTypes.PERSISTENT_MINI}
                                  desktopDrawerType={NavigationDrawer.DrawerTypes.PERSISTENT_MINI}
                                  navItems={navItems.map(props => <NavLink  label={props.label} to={props.to} exact={props.exact} icon={props.icon}  key={props.to} />)}
                                  contentId="main-content"
                                  contentClassName="md-grid"
                                >
                                  <Switch key={location.pathname}>
                                    <Route path={navItems[0].to} exact component={Home} />
                                    <Route path={navItems[1].to} component={Users} />
                                
                                  </Switch>
                                </NavigationDrawer>)} />
    )
}
}

const LayoutWrapped = withRouter(Layout)
export  { LayoutWrapped  as Layout} 