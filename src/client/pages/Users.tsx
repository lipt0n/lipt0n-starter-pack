import * as React from "react"
import { graphql } from 'react-apollo'
import { Card, CardTitle, CardText, TextField,
        Grid, Cell, Button
    } from 'react-md'
import { compose } from 'react-apollo'

import * as getUsers from 'client/graphql/users.graphql'
import * as getUsersSub from 'client/graphql/users_sub.graphql'

import * as createUser from 'client/graphql/createuser.graphql'

class Users extends React.PureComponent<any,any> {
    wrap = text=>{
        return <Card>
            <CardTitle title='Users' />
            <CardText>{text}</CardText>
        </Card>
    }
    createUser = ()=>{
        const {firstName, lastName, age} = this.state
        this.props.createUser({variables:{
            firstName,
            lastName,
            age:parseInt(age)
        }})
    }
    render(){
        const {users, usersSub} = this.props
        if(users.error) return this.wrap(<h1>ERROR:{users.error}</h1>)
        if(users.loading) return this.wrap(<h1>loading ...</h1>)
        if(users.users){
          const data = !usersSub.loading && usersSub.users.length>0 ? usersSub.users : users.users
        return  <div>
                    <Card>
                        <CardText>
                            <Grid>
                                <Cell size={3}>
                                    <TextField
                                    id='input_firstName'
                                    label='First name'
                                    onChange={v=>this.setState({firstName:v})}
                                    />
                                </Cell>
                                <Cell size={3}>
                                    <TextField
                                    id='input_lastName'
                                    label='Last name'
                                    onChange={v=>this.setState({lastName:v})}
                                    />
                                </Cell>
                                <Cell size={3}>
                                    <TextField
                                    id='input_age'
                                    label='age'
                                    onChange={v=>this.setState({age:v})}
                                    />
                                </Cell>
                                <Cell size={3} style={{position:'relative'}}>
                                    <Button
                                    primary
                                    flat

                                    style={{
                                        position:'absolute',
                                        bottom:0,
                                        marginLeft:10
                                    }}
                                    onClick={()=>this.createUser()}
                                    > add user ! </Button>
                                </Cell>
                            </Grid>
                        </CardText>
                    </Card>
            {this.wrap(<div >
                        {data.map((u,i)=><div key={i}>{u.firstName} {u.lastName}</div>)}
                    </div>)}

                </div>
              }
        return this.wrap('')
    }
}

const _Users = compose(
    //graphql(getUsers, {name:'users', options: {pollInterval:2000}}),
    graphql(getUsers, {name:'users'}),
    graphql(getUsersSub, {name:'usersSub'),
    graphql(createUser, {name:'createUser'})

)(Users)
export {_Users as Users}
