import * as React from "react"
import { Card, CardTitle, CardText } from 'react-md'
const Home =  ()=>{

    return <Card>
        <CardTitle title='Home' />
        <CardText>my starter pack</CardText>
    </Card>
}
export {Home}